# Growth Product project

This project is deprecated in favor of:

* [Team Tasks](https://gitlab.com/gitlab-org/growth/team-tasks) for Growth team specific tasks.
* [GitLab.org](https://gitlab.com/gitlab-org/) for issues relating to projects in gitlab-org (including [GitLab](https://gitlab.com/gitlab-org/))
* [GitLab Services](https://gitlab.com/gitlab-services) for issues relating to projects in gitlab-services (including [Versions](https://gitlab.com/gitlab-services/version-gitlab-com/))